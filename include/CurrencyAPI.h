#ifndef CURRENCYAPI_H
#define CURRENCYAPI_H
#include <mutex>
#include <json.hpp>
#include <unordered_map>
#include <memory>
#include <atomic>
#include <vector>

constexpr uint8_t TickerSize = 3;

typedef std::unordered_map<std::string, double> BaseMap;
typedef std::shared_ptr<BaseMap> BaseMapPtr;

typedef std::unordered_map<std::string, BaseMapPtr> RatesMap;
typedef std::shared_ptr<RatesMap> RatesMapPtr;

class CurrencyAPI {
public:
	CurrencyAPI(json::JSON const & config);
 	void UpdateValues(std::string const & base, std::string const &unparsedJSON);
	void SetNewDataAvailable(bool const isAvailable);
	RatesMap GetMapCopy();

	bool IsNewDataAvailable();

	static void SortVectorAlphabetically(std::vector<std::string> &vec);

	size_t NbCurrencies() const { return _currenciesMap->size(); }

	auto Begin() { 
		std::lock_guard<std::mutex> lock(_mtx);
		return  _currenciesMap->begin(); 
	}

	auto End() { 
		std::lock_guard<std::mutex> lock(_mtx);
		return  _currenciesMap->end(); 
	}


	BaseMapPtr MakeBaseMapPtr() {
		return std::make_shared<BaseMap>();
	}

	BaseMapPtr MakeBaseMapPtr(BaseMap & map) {
		return std::make_shared<BaseMap>(map);
	}

	RatesMapPtr MakeRatesMapPtr() {
		return std::make_shared<RatesMap>();
	}

protected:
	void _initRatesMap(json::JSON const &currencies);
	BaseMapPtr _getBaseMap(std::string const &base);
	void _addCurrency(std::string const &base, BaseMapPtr baseMap);
	void _updateCurrency(std::string const &base, BaseMapPtr baseMap);
	RatesMapPtr _currenciesMap;
	std::mutex _mtx;
	json::JSON _valueJSON;
	std::atomic<bool> _newDataAvailable;
};


#endif
#include <include/CurrencyAPI.h>
#include <include/SysLog.h>
#include <algorithm>

CurrencyAPI::CurrencyAPI(json::JSON const &config) :
_newDataAvailable(false) {

	_currenciesMap = MakeRatesMapPtr();
	_initRatesMap(config.at("Currencies"));
}

void CurrencyAPI::SortVectorAlphabetically(std::vector<std::string> &vec) {
	std::sort(vec.begin(), vec.end(), [](const std::string &lhs, const std::string &rhs)
		{
			return lhs < rhs;
		});
}

void CurrencyAPI::_initRatesMap(json::JSON const &currencies) {
    std::lock_guard<std::mutex> lock(_mtx);
	std::vector<std::string> currenciesVector;
	BaseMapPtr fullMap = MakeBaseMapPtr();

	for (uint16_t i = 0; i < currencies.length(); ++i)
		currenciesVector.emplace_back(currencies.at(i).ToString());

	SortVectorAlphabetically(currenciesVector);

	for (auto &r :currenciesVector)
		fullMap->insert({ r, 0.0 });

	for (uint16_t i = 0; i < currencies.length(); ++i) {
		BaseMap mapCopy = *fullMap;
		mapCopy.erase(currencies.at(i).ToString());

		_addCurrency(currencies.at(i).ToString(), MakeBaseMapPtr(mapCopy));
	}

#ifndef NDEBUG
	SysLog::Log(SysLog::SeverityLevel::Debug, "System configured for following currencies:");
	for (auto it = _currenciesMap->begin(); it != _currenciesMap->end(); ++it)
		SysLog::Log(it->first);
#endif // !NDEBUG

}

BaseMapPtr CurrencyAPI::_getBaseMap(std::string const &base) {
	return _currenciesMap->at(base);
}

void CurrencyAPI::_addCurrency(std::string const &base, BaseMapPtr baseMap) {
	_currenciesMap->insert({ base, baseMap});
}

void CurrencyAPI::_updateCurrency(std::string const &base, BaseMapPtr baseMap) {
	_currenciesMap->at(base) = baseMap;
}

void CurrencyAPI::UpdateValues(std::string const & base, std::string const & unparsedJSON) {
	std::lock_guard<std::mutex> guard(_mtx);

#ifndef NDEBUG 
	std::cout << json::JSON::Load(unparsedJSON);
#endif // !NDEBUG

	_valueJSON = json::JSON::Load(unparsedJSON).at("rates");

	auto baseMap = _getBaseMap(base);

	for (auto its = baseMap->begin(); its != baseMap->end(); ++its) {
		its->second = _valueJSON.at(its->first).ToFloat();
	}

#ifndef NDEBUG
	SysLog::Log(SysLog::SeverityLevel::Debug, "Currencies values udpdated", true);
#endif // !NDEBUG

}

void CurrencyAPI::SetNewDataAvailable(bool const isAvailable) {
	_newDataAvailable = isAvailable;
}

RatesMap CurrencyAPI::GetMapCopy() {
	std::lock_guard<std::mutex> mtx(_mtx);

#ifndef NDEBUG
	SysLog::Log(SysLog::SeverityLevel::Debug, "Currencies values read", true);
#endif // !NDEBUG

	RatesMap copy = *_currenciesMap;
	_newDataAvailable = false;
	return copy;
}

bool CurrencyAPI::IsNewDataAvailable() {
#ifndef NDEBUG
	SysLog::Log(SysLog::SeverityLevel::Debug, "Checking if new data is available ", true);
#endif // !NDEBUG

	return _newDataAvailable;
}
